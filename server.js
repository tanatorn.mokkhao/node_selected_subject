const express = require('express')
const app = express()
require('dotenv').config()
const mongooseConnection = require("./src/mongoose/mongoose.connect")
const TestRoute = require('./src/route/test.route')
const CategoryRoute = require('./src/route/category.route')
const ProductRoute = require('./src/route/product.route')
const cors = require('cors')

//connect mongoose
mongooseConnection()

//middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// route
app.use('/tests', TestRoute)
app.use('/categories', CategoryRoute)
app.use('/products', ProductRoute)

app.listen(process.env.PORT, () => {
  console.log("server is running on port", process.env.PORT)
});