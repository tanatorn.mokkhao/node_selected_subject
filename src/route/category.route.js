const express = require('express')
const router = express.Router()
const CategoryController = require('../controller/category/category.controller')

router.post('/', CategoryController.CreatCategory)

module.exports = router